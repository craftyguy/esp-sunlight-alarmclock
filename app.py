# Copyright(c) 2017 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see COPYING) WITHOUT ANY WARRANTY.

import uasyncio as asyncio
import asyn
import machine
from umqtt.simple import MQTTClient

# Settings
PROJECT = "esp-sunlight-alarm"
MQTT_SERVER = '1.1.1.1'     # MQTT Broker to subscribe with
PWM_FREQUENCY = 1000            # in Hz, should be 0 < freq < 1000 for esp8266
START_TOPIC = 'home/bedroom/master/alarm/start'
STOP_TOPIC = 'home/bedroom/master/alarm/stop'
STATUS_TOPIC = 'home/bedroom/master/alarm/status'

### MQTT Topics used, with expected parameters:
## START_TOPIC
### Params: start_illum:stop_illum:lit_dur:dim_dur
## STOP_TOPIC
### Params: <none>
## STATUS_TOPIC
### Params: <none>

### Globals
alarm_stop = False
alarm_going = False
# Coroutine loop
loop = asyncio.get_event_loop()
# Generate unique name based on uid and start mqtt client
umqtt_uid = PROJECT + "_" + ''.join('%02X' % b for b in machine.unique_id())
umqtt_client = MQTTClient(umqtt_uid, MQTT_SERVER)
# lock used to restrict access to umqtt_client
umqtt_lock = asyn.Lock()
mqtt_connected = False

# Start light alarm dimming
# 0 <= start_illum < stop_illum < 100
# durations are in minutes
async def start_alarm(start_illum, stop_illum, lit_dur, dim_dur):
    global alarm_stop
    global alarm_going
    alarm_stop = False

    if alarm_going:
        return
    else:
        alarm_going = True

    # On the esp8266, any value less than about 14% duty cycle doesn't seem to
    # allow enough power to light the LEDs with PWM, so 14% is the bottom
    if start_illum > stop_illum:
        start_illum = 14
    if start_illum < 14:
        start_illum = 14
    # Keep illums within bounds
    if stop_illum > 100:
        stop_illum = 100
    if stop_illum < start_illum:
        stop_illum = start_illum

    print("Start/Stop illumination: %i%% / %i%%, Dim duration: %im, duration at max: %im" % (start_illum,
            stop_illum, dim_dur, lit_dur))

    # convert start and stop illumination %s into corresponding duty cytle (max == 1024)
    start_int = int((1024 * start_illum) / 100)
    stop_int = int((1024 * stop_illum) / 100)
    cur_duty = start_int
    # In this method, step is always 1, and the amount of time to wait (in ms)
    # between steps is calcuated ahead of time
    step = 1
    step_sleep = int(dim_dur * 60 * 1000)
    # Protect against div by 0
    if stop_int != start_int:
        step_sleep = int(step_sleep / (stop_int - start_int))

    #print("step_sleep: %s, start_int: %s, stop_int: %s" % (step_sleep, start_int, stop_int))
    # set up PWM pin (using #2)
    p2 = machine.Pin(2)
    pwm2 = machine.PWM(p2)
    pwm2.freq(PWM_FREQUENCY)

    # stop_alarm() will set alarm_stop if approp. mqtt topic is received
    while not alarm_stop:
        pwm2.duty(cur_duty)
        cur_duty += step
        await asyncio.sleep_ms(step_sleep)
        # check if alarm is done ramping
        if cur_duty >= stop_int or cur_duty >= 1024:
            print("Alarm done!")
            print("At max illumination for this alarm!")
            break
    # Alarm is finished dimming, now to hold max specified illumination for
    # specified duration, while listenning for a command to stop.
    # reuse lit_dur as a counter, after converting to seconds
    lit_dur *= 60
    while not alarm_stop and lit_dur >= 0:
        await asyncio.sleep(1)
        lit_dur -= 1

    # reset light
    print("Turning off light")
    p2.value(0)
    pwm2.deinit()
    alarm_going = False

# Signal to stop alarm
async def stop_alarm():
    global alarm_stop
    alarm_stop = True
    print("Alarm has been stopped!")

# publish alarm status loop
async def publish_status(lock):
    global umqtt_client
    global mqtt_connected
    while True:
        async with lock:
            if mqtt_connected:
                try:
                    if alarm_going:
                        umqtt_client.publish(STATUS_TOPIC, b'on')
                    else:
                        umqtt_client.publish(STATUS_TOPIC, b'off')
                except:
                    loop.create_task(reconnect(lock))
        await asyncio.sleep(30)

# mqtt callback for new subscription message
def sub_cb(topic, msg):
    t = topic.decode('ASCII')
    m = msg.decode('ASCII')
    print("received new topic/msg: %s / %s" % (t, m))
    loop = asyncio.get_event_loop()

    if t == START_TOPIC:
        try:
            start_illum, stop_illum, lit_dur, dim_dur = m.split(';', 4)
            # Try to convert everything to int so any exceptions can be caught
            start_illum = int(start_illum)
            stop_illum = int(stop_illum)
            lit_dur = int(lit_dur)
            dim_dur = int(dim_dur)
        except Exception as e:
            print("Unable to parse parameters from message..")
            print(e)
            return
        loop.create_task(start_alarm(int(start_illum), int(stop_illum),
                        int(lit_dur), int(dim_dur)))
    elif t == STOP_TOPIC:
        loop.create_task(stop_alarm())
    else:
        print("Unknown topic")

# This function handles reconnection to broker in the event that the broker
# is suddenly disconnected. It's also the primary way a connection is first
# established when the app starts.
def reconnect(lock):
    global umqtt_client
    global mqtt_connected
    # delay in seconds for reconnect retry
    retry_delay = 5
    # umqtt client parameters
    umqtt_client.DEBUG = True
    umqtt_client.DELAY = 5
    umqtt_client.set_callback(sub_cb)
    mqtt_connected = False
    async with lock:
        while not mqtt_connected:
            try:
                umqtt_client.connect(clean_session=True)
                mqtt_connected = True
                umqtt_client.subscribe(START_TOPIC)
                umqtt_client.subscribe(STOP_TOPIC)
                print("Connected to MQTT broker: %s" % MQTT_SERVER)
            except:
                # Unable to connect to mqtt server, so try again after waiting
                print("Unable to connect to mqtt server %s, trying again in %i seconds" % (MQTT_SERVER, retry_delay))
                await asyncio.sleep(retry_delay)

# Loop for checking mqtt subscriptions for messages
async def check_subs(lock):
    global umqtt_client
    global mqtt_connected

    while True:
        # non-blocking check for messages to allow other coros to do their thang
        async with lock:
            if mqtt_connected:
                try:
                    umqtt_client.check_msg()
                except:
                    loop.create_task(reconnect(lock))
        await asyncio.sleep(3)
    # should never get here, but just in case:
    async with lock:
        umqtt_client.disconnect()


def main():
    p = machine.Pin(2, machine.Pin.OUT)
    p.value(0)
    loop.create_task(reconnect(umqtt_lock))
    loop.create_task(check_subs(umqtt_lock))
    loop.create_task(publish_status(umqtt_lock))
    loop.run_forever()
