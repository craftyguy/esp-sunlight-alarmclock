# esp-sunlight-alarmclock
A "Sun Light" Alarm Clock using an ESP8266 and MQTT


### Summary:
The ESP266 operates as a MQTT client, listenning for topics to start and stop a bank of LEDs that it is controlling via PWM. In order to make a complete alarm clock, you will need an MQTT broker and some other client (e.g. openHAB) to publish commands at scheduled times. The ESP8266, in this case, is really just a limited dimmer controlled over MQTT.



### Description:
This is a MQTT client for controlling a bank of LEDs running on an ESP8266 microcontroller. It uses the [micropython for the ESP8266](https://github.com/micropython/micropython/tree/master/esp8266), and two libraries (uasyncio and umqtt) from [micropython-lib](https://github.com/micropython/micropython-lib).

In this configuration, the ESP8266 is intended to listen for three MQTT topics, `home/bedroom/master/alarm/start`, `home/bedroom/master/alarm/stop`, and `home/bedroom/master/alarm/status`. These topics are customizable by setting the appropriate variable at the beginning of the `light_alarm.py` file.

* `home/bedroom/master/alarm/start` requires a message in the form: `A;BBB;CC;DD`, where all values, except the colons, are integers. `A` and `BBB` are integers that indicate the starting and stopping light intensity, respectively, as a percentage. On the ESP8266, any percentage less than 14% seems to operate PWM too low to light any substantial LEDs, so the min value used is 14%. `CC` and `DD` are integers that represent the number of minutes to keep the light at the designated max intensity and the number of minutes to take to reach that max intensity from the designated min, respectively. For example, to go between 20% and 80% intensity over 5 minutes, and keep it at 80% for 10 minutes, you would send this message: `20;80;10;5`

Example:

    $ mosquitto_pub -h 10.1.1.1 -t home/bedroom/master/alarm/start -m '20;80;10;5'

* `home/bedroom/master/alarm/stop` does not expect any message, nor does it act differently if one is given. Publishing *anything* to this topic will cause the light alarm to stop and shut off the light.

Example: 

    $ mosquitto_pub -h 10.1.1.1 -t home/bedroom/master/alarm/stop -m ''

* `home/bedroom/master/alarm/status` is the topic where the device will send either `on` or `off`, indicating whether or not the light alarm is actively running (i.e. light is on). This message is sent every 1 minute, but the interval is configurable via a variable at the top of the `light_alarm.py` file.

Example:

    $ mosquitto_sub -h 10.1.1.1 -t home/bedroom/master/alarm/status
    on
    off

### Installation:

- [Build the esp-open-sdk](https://github.com/pfalcon/esp-open-sdk).

- Download micropython-lib and micropython from their respective project pages.

- [Include uasyncio and umqtt modules as frozen modules in micropython](https://learn.adafruit.com/micropython-basics-loading-modules/frozen-modules)
  - Note: Building uasyncio as frozen bytecode is no longer necessary in Micropython 1.9, since it is included by default!

- Also include [micropython-async](https://github.com/peterhinch/micropython-async) (asyn.py) as a frozen module. Note: if you run out of space, consider removing the web_repl applications, or prune something else that is not a requirement here.

- Install `main.py` and `boot.py` from the [esp-bootstrap](https://github.com/craftyguy/esp-bootstrap) project. Also pay attention to instructions for creating the `secrets.py` file, which also needs to be installed on the device.

- Build micropython. I suggest including the files in this project under `esp8266/scripts` in your micropython directory so you don't have to manually copy these files to the device

  - Hint: You'll need python2 env. for esptool.py to run successfully at the end of the build..


- [Flash the ESP8266 using esptool](https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/intro.html#intro), using your fresh firmware-combined.bin image of course!

Power on device and send it a command! Here's an example with [Mosquitto](http://mosquitto.org/):

    mosquitto_pub -h 10.1.1.1 -t home/bedroom/master/alarm/start -m "0;100;15;10"



For the hardware, I used a [design very similar to this one](https://github.com/Jeija/esp8266-light-alarmclock), but used a MOSFET instead of the PNP & NPN transistors. I have tested this on both an Adafruit Huzzah and the Wemos D1 Mini.
